<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class WordPressServiceProvider extends ServiceProvider
{
     protected $bootstrapFilePath = '../../wp-load.php';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
     
        //

       
       wp_enqueue_style('app', '/app/public/app.css');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        // if(file::exists($this->bootstrapFilePath)) {
        //     require_once $this->bootstrapFilePath;
        // } else throw new \RuntimeException('WordPress Bootstrap file not found!');
    }
}
